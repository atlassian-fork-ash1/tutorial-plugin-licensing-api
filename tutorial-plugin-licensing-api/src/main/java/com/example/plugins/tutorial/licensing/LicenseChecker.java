package com.example.plugins.tutorial.licensing;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.PluginController;
import com.atlassian.upm.api.license.PluginLicenseEventRegistry;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.LicenseError;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.license.event.PluginLicenseChangeEvent;
import com.atlassian.upm.api.license.event.PluginLicenseCheckEvent;
import com.atlassian.upm.api.license.event.PluginLicenseRemovedEvent;
import com.atlassian.upm.api.util.Option;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.upm.api.util.Option.none;
import static com.atlassian.upm.api.util.Option.some;

/**
 * Listens for Atlassian licensing events, and disables this plugin whenever it does not
 * have a valid license.
 */
public class LicenseChecker {

    private static final Logger log = LoggerFactory.getLogger(LicenseChecker.class);

    private final PluginController pluginController;
    private final PluginLicenseManager pluginLicenseManager;
    private final String pluginKey;
    
    public LicenseChecker(PluginController pluginController,
                              PluginLicenseManager pluginLicenseManager,
                              PluginLicenseEventRegistry pluginLicenseEventRegistry) {
        this.pluginController = pluginController;
        this.pluginLicenseManager = pluginLicenseManager;
        this.pluginKey = pluginLicenseManager.getPluginKey();
        
        pluginLicenseEventRegistry.register(this);
    }

    /**
     * This event is generated when the plugin becomes enabled - either at application
     * startup time, or when the plugin is installed into an already-running application.
     * The plugin may or may not have a license at this point.
     */
    @EventListener
    public void handleEvent(PluginLicenseCheckEvent event) {
        System.out.println("HANDING CHECK EVENT: " + event);
        checkLicense(event.getLicense());
    }
    
    /**
     * This event base class includes all changes to the plugin license other than its
     * complete removal.
     */
    @EventListener
    public void handleEvent(PluginLicenseChangeEvent event) {
        System.out.println("HANDING CHANGE EVENT: " + event);
        checkLicense(some(event.getLicense()));
    }

    /**
     * This event is generated if an existing license for the plugin is removed.
     */
    @EventListener
    public void handleEvent(PluginLicenseRemovedEvent event) {
        System.out.println("HANDING REMOVE EVENT: " + event);
        checkLicense(none(PluginLicense.class));
    }
    
    private final void checkLicense(Option<PluginLicense> maybeLicense) {
        if (!isValidLicense(maybeLicense)) {
            log.warn("Disabling plugin '" + pluginKey + "'");
            pluginController.disablePlugin(pluginKey);            
        }
    }
    
    private final boolean isValidLicense(Option<PluginLicense> maybeLicense) {
        for (PluginLicense license: maybeLicense) {
            if (!license.isValid())
            {
                log.warn("Invalid license for plugin \"" + pluginKey + "\" (" + license.getError().getOrElse((LicenseError) null) + ")");
                return false;
            }
            log.info("Validated license for plugin '" + pluginKey + "'");
            return true;
        }
        log.warn("No license available for plugin '" + pluginKey + "'");
        return false;
    }

    /**
     * Validates the plugin's license. Disables the plugin if the license is invalid.
     * Returns true if the license is valid, false if not.
     * @return true if the license is valid, false if not.
     */
    public boolean validateLicense() {
        Option<PluginLicense> license = pluginLicenseManager.getLicense();
        checkLicense(license);
        return isValidLicense(license);
    }
}
